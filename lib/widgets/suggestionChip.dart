import 'package:flutter/material.dart';

class SuggestionChip extends StatelessWidget {

  final String label;
  final Function onSelected;

  SuggestionChip({@required this.label, @required this.onSelected});
  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      selected: false,
      backgroundColor: Colors.deepOrange,
      label: Text(this.label),
      onSelected: this.onSelected,
    );
  }
}