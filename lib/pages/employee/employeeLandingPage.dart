import 'package:flutter/material.dart';
// import 'package:google_sign_in/google_sign_in.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/employee/employeeLoginPage.dart';
import 'package:hope/pages/employee/employeeRegistrationForm.dart';

// final GoogleSignIn googleSignIn = GoogleSignIn(); 

class EmployeeLandingPage extends StatefulWidget {
  @override
  _EmployeeLandingPageState createState() => _EmployeeLandingPageState();
}

class _EmployeeLandingPageState extends State<EmployeeLandingPage> {

  /* login(){
    googleSignIn.signIn();
  } */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate('all_the_best'),
                        style: TextStyle(
                          fontSize: 60,
                          fontFamily: 'Montserrat',
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .translate('lets_get_started'),
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrangeAccent,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('login'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EmployeeLoginPage()));
                  },
                ),
                /* BorderButton(
                  label: AppLocalizations.of(context).translate('login'),
                  onPressed: login,
                ), */
                BorderButton(
                  label: AppLocalizations.of(context).translate('new_user'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EmployeeRegistrationForm()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
