import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:location/location.dart';

class LocationPicker extends StatefulWidget {
  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  // Position _currentPosition;
  Location _location;
  LocationData _locationData;
  double _latitude = 21.494069;
  double _longitude = 78.456259;
  GoogleMapController mapController;
  LatLng _temp;
  Map<String, double> userLocation;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  // _getCurrentLocation() async {
  //   _currentPosition =
  //       await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  //   print(
  //       "Location is ${_currentPosition.latitude}, ${_currentPosition.longitude}");
  // }

  _getLocation() async {
    _location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await _location.getLocation();
    print("This is my location data: ${_locationData}");
  }

  @override
  Widget build(BuildContext context) {
    _getLocation();
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              AppLocalizations.of(context).translate('add_location'),
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 2,
                  child: GoogleMap(
                    onMapCreated: _onMapCreated,
                    onCameraMove: (object) {
                      _temp = object.target;
                    },
                    initialCameraPosition: CameraPosition(
                      target: LatLng(_latitude, _longitude),
                      zoom: 4.5,
                    ),
                  ),
                ),
                Icon(
                  Icons.center_focus_strong,
                  color: Colors.red,
                  size: 30,
                ),
              ],
            ),
          ),
          BorderButton(
            label: AppLocalizations.of(context).translate('map_location'),
            onPressed: () {
              Navigator.pop(context, [_temp.latitude, _temp.longitude]);
            },
          ),
          BorderButton(
            label: AppLocalizations.of(context).translate('current_location'),
            onPressed: () {
              print("I am printing");
              print(
                  "Location is ${_locationData.latitude}, ${_locationData.longitude}");
              Navigator.pop(
                  context, [_locationData.latitude, _locationData.longitude]);
            },
          ),
        ],
      ),
    ));
  }
}
