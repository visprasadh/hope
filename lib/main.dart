import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hope/models/customUser.dart';
import 'package:hope/services/firestoreService.dart';
import 'package:provider/provider.dart';
import 'package:hope/widgets/authWrapper.dart';
import 'package:hope/services/authService.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:firebase_core/firebase_core.dart';

import 'models/job.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Hope());
}

class Hope extends StatefulWidget {
  @override
  _HopeState createState() => _HopeState();
}

class _HopeState extends State<Hope> {
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        StreamProvider<CustomUser>.value(value: AuthService().userStream, initialData: null),
        StreamProvider<List<Job>>.value(value: FirestoreService().jobs, initialData: null)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Hope',
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.deepOrange,
          accentColor: Colors.deepOrangeAccent,
          fontFamily: 'Raleway',
        ),
        darkTheme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.deepOrange,
          accentColor: Colors.deepOrangeAccent,
          fontFamily: 'Raleway',
        ),
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), //English
          const Locale('fr'), //French
          const Locale('ta'), //Tamil
          const Locale('hi'), //Hindi
        ],
        home: AuthWrapper(),
      ),
    );
  }
}
