
# Hope

The Hopeful Platform for Employment is an online portal that aims to help connect migrant workers displaced as a result of the Covid-19 pandemic with businesses looking to employ workers within the same geographical region.

# Features
## For Job Seekers

 - The migrant workers will neither have to travel back to his/her earlier place immediately, nor rush back after lockdown is lifted.
 - One has to register for Job, with phone number, Aadhar number.
 - A migrant worker can share his previous location or current location.
 - Job search option in nearby location (GPS enabled)

## For Employers

 - An employer can upload his job openings of any kind (community works, Hotel House Keeping, data entry, up to Construction work, MNREGA etc.) by sharing the current location( GPS)
 - An Employer/ govt officials can also search for migrant employees in the nearby location( GPS enabled).

## Features that can be added

 - If the worker does not have a smartphone, they can walk to the nearest Panchayat in the locality to do the job search (Admin Mode).
 - If the user was availing any rations earlier, the same can be distributed in the new location ( as Ration Cards are Aadhar enabled).
 - This app can be merged with the **Arogya Setu** app in order to ensure that the worker is in safe surroundings.
 - OTP Authentication for Registration.

# Installing

To install the latest version of Hope, head on over to the releases page and download the latest build or choose the latest build from the links below.

# Releases

- Hope v1.0.0 - [APK](https://gitlab.com/HeartwinJ/hope/-/raw/master/releases/v1.0.0/hope-v1.0.0.apk)

- Hope v1.0.1 - [APK](https://gitlab.com/HeartwinJ/hope/-/raw/master/releases/v1.0.1/hope-v1.0.1.apk)

- Hope v1.0.2 - [APK](https://gitlab.com/HeartwinJ/hope/-/raw/master/releases/v1.0.2/hope-v1.0.2.apk)