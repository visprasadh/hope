import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String title;
  final Function onPressed;

  Header({@required this.title, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              child: IconButton(
                icon: Icon(Icons.menu, size: 26),
                onPressed: onPressed,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
