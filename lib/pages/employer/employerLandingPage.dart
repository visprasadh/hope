import 'package:flutter/material.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/pages/employer/employerLoginPage.dart';
import 'package:hope/pages/employer/employerRegistrationForm.dart';
import 'package:hope/models/app_localizations.dart';

class EmployerLandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate('awesome'),
                        style: TextStyle(
                          fontSize: 60,
                          fontFamily: 'Montserrat',
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .translate('lets_get_started'),
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.deepOrangeAccent,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('login'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EmployerLoginPage()));
                  },
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('new_user'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EmployerRegistrationForm()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
