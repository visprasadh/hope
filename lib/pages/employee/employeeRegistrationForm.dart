import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EmployeeRegistrationForm extends StatefulWidget {
  @override
  _EmployeeRegistrationFormState createState() =>
      _EmployeeRegistrationFormState();
}

class _EmployeeRegistrationFormState extends State<EmployeeRegistrationForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  String _name;
  String _occupation;
  String _phoneNum;
  String _aadharNum;
  String _prevEmployerName;
  String _prevEmployerNum;
  String _district;

  void _vaildateRegistration() async {
    print('Validating...');
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      await _authService
          .signUpEmployeeWithGoogle(_name, _occupation, _phoneNum, _aadharNum,
              _prevEmployerName, _prevEmployerNum, _district)
          .then((user) => {
                if (user != null)
                  {Navigator.popUntil(context, (route) => route.isFirst)}
                else
                  {
                    // TODO: Handle the errors properly
                    print('An Error Occured!')
                  }
              });
    }
  }

  void _save() async {
    final pref = await SharedPreferences.getInstance();
    final key = 'district';
    final value = _district;
    pref.setString(key, value);
    print('District saved: $value');
  }

  @override
  Widget build(BuildContext context) {
    List<String> _districts = [
      AppLocalizations.of(context).translate("Puducherry"),
      AppLocalizations.of(context).translate("Karaikal"),
      AppLocalizations.of(context).translate("Yanam"),
      AppLocalizations.of(context).translate("Mahe"),
    ];
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: _formKey,
        autovalidate: _autovalidate,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                AppLocalizations.of(context).translate('employee_registration'),
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('name'),
                      ),
                      onSaved: (input) {
                        _name = input;
                      },
                      validator: (input) => (input.isEmpty)
                          ? AppLocalizations.of(context).translate('required')
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context)
                            .translate('occupation'),
                      ),
                      onSaved: (input) {
                        _occupation = input;
                      },
                      validator: (input) => (input.isEmpty)
                          ? AppLocalizations.of(context).translate('required')
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('phone_no'),
                      ),
                      onSaved: (input) {
                        _phoneNum = '+91' + input;
                      },
                      validator: (input) => (input.length == 10)
                          ? null
                          : AppLocalizations.of(context)
                              .translate('valid_phone'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('aadhar'),
                      ),
                      onSaved: (input) {
                        _aadharNum = input;
                      },
                      validator: (value) =>
                          (value.isEmpty || value.length == 12)
                              ? null
                              : AppLocalizations.of(context)
                                  .translate('valid_aadhar'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context)
                            .translate('prev_emp_name'),
                      ),
                      onSaved: (input) {
                        _prevEmployerName = input;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context)
                            .translate('prev_emp_phone'),
                      ),
                      onSaved: (input) {
                        _prevEmployerNum = '+91' + input;
                      },
                      validator: (input) =>
                          (input.isEmpty || input.length == 10)
                              ? null
                              : AppLocalizations.of(context)
                                  .translate('valid_phone'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 2),
                        child: DropdownButton(
                            underline: SizedBox(),
                            itemHeight: 50,
                            iconEnabledColor: Colors.deepOrange,
                            hint: Text('Select district'),
                            items: _districts.map((location) {
                              return DropdownMenuItem<String>(
                                child: Text(location),
                                value: location,
                              );
                            }).toList(),
                            value: _district,
                            onChanged: (String newValue) {
                              setState(() => _district = newValue);
                              _save();
                            }),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: BorderButton(
                label: AppLocalizations.of(context).translate('submit'),
                onPressed: () => _vaildateRegistration(),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
