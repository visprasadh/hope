import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hope/models/job.dart';
import 'package:hope/models/userTypes.dart';

class FirestoreService {
  final CollectionReference _users = FirebaseFirestore.instance.collection('users');
  final CollectionReference _jobs = FirebaseFirestore.instance.collection('jobs');

  Future updateEmployer(String uid, String name, String address,
      String phoneNum, String aadharNum, UserTypes userType) async {
    try {
      _users.doc(uid).set({
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'aadharNum': aadharNum,
        'userType': (userType == UserTypes.EMPLOYER) ? 'EMPLOYER' : 'EMPLOYEE'
      });
    } catch (error) {
      print(error.toString());
    }
  }

  Future updateEmployee(
      String uid,
      String name,
      String occupation,
      String phoneNum,
      String aadharNum,
      String prevEmployerName,
      String prevEmployerNum,
      String district,
      UserTypes userType) async {
    try {
      _users.doc(uid).set({
        'name': name,
        'occupation': occupation,
        'phoneNum': phoneNum,
        'aadharNum': aadharNum,
        'prevEmployerName': prevEmployerName,
        'prevEmployerNum': prevEmployerNum,
        'district': district,
        'userType': (userType == UserTypes.EMPLOYER) ? 'EMPLOYER' : 'EMPLOYEE'
      });
    } catch (error) {
      print(error.toString());
    }
  }

  Future<DocumentSnapshot> getUserDocument(String uid) async {
    DocumentSnapshot doc;
    try {
      await _users.doc(uid).get().then((result) => doc = result);
    } catch (error) {
      print(error.toString());
    }
    return doc;
  }

  Future postJob(
      String uid,
      String title,
      String description,
      String name,
      String address,
      String phoneNum,
      String email,
      double lat,
      double long,
      String district) async {
    try {
      await _jobs.add({
        'postedBy': uid,
        'title': title,
        'description': description,
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'email': email,
        'latitude': lat,
        'longitude': long,
        'district': district,
      });
    } catch (error) {
      print(error.toString());
    }
  }

  List<Job> _createJobFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return Job(
          title: doc['title'],
          description: doc['description'],
          name: doc['name'],
          address: doc['address'],
          phone: doc['phoneNum'],
          email: doc['email'],
          lat: doc['latitude'],
          long: doc['longitude'],
          postedBy: doc['postedBy'],
          district: doc['district'],
          id: doc.id);
    }).toList();
  }

  Stream<List<Job>> get jobs {
    return _jobs
        .snapshots()
        .map((snapshot) => _createJobFromSnapshot(snapshot));
  }

  Future deleteJob(String jobID) async {
    return await _jobs.doc(jobID).delete();
  }

  Future updateJob(
      String uid,
      String title,
      String description,
      String name,
      String address,
      String phoneNum,
      String email,
      double lat,
      double long,
      String district,
      String jobID) async {
    try {
      await _jobs.doc(jobID).update({
        'postedBy': uid,
        'title': title,
        'description': description,
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'email': email,
        'latitude': lat,
        'longitude': long,
        'district': district,
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
