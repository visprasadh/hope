import 'package:flutter/material.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/widgets/header.dart';
import 'package:hope/models/app_localizations.dart';

class Settings extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Header(
              title: AppLocalizations.of(context).translate('settings'),
              onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  FlatButton(
                    child: Text(
                        AppLocalizations.of(context).translate('lang_set')),
                    onPressed: null,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      key: _scaffoldKey,
      endDrawer: Drawer(
        child: DrawerContents(),
      ),
    );
  }
}
