import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class EmployerLoginPage extends StatefulWidget {
  @override
  _EmployerLoginPageState createState() => _EmployerLoginPageState();
}

class _EmployerLoginPageState extends State<EmployerLoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  /* String _email;
  String _password; */

  /* String _emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty)
      return AppLocalizations.of(context).translate('required');
    if (!regex.hasMatch(value))
      return AppLocalizations.of(context).translate('valid_email');
    else
      return null;
  } */

  void _validateLogIn() async {
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      await _authService.signInWithGoogle().then((user) => {
            if (user != null)
              {Navigator.popUntil(context, (route) => route.isFirst)}
            else
              {
                // TODO: Handle Errors properly
                print('An Error Occured!')
              }
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 100),
            width: double.infinity,
            child: Center(
              child: Text(
                AppLocalizations.of(context).translate('great_to'),
                style: TextStyle(
                  fontSize: 40,
                  fontFamily: 'Montserrat',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    /* Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText:
                              AppLocalizations.of(context).translate('email'),
                        ),
                        onSaved: (input) {
                          _email = input;
                        },
                        validator: _emailValidator,
                      ),
                    ), */
                    /* Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20),
                      child: TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('password'),
                        ),
                        onSaved: (input) {
                          _password = input;
                        },
                        validator: (value) => (value.isNotEmpty)
                            ? null
                            : AppLocalizations.of(context)
                                .translate('not_empty'),
                      ),
                    ), */
                    BorderButton(
                      label: AppLocalizations.of(context).translate('login'),
                      onPressed: _validateLogIn,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
