import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class EmployerRegistrationForm extends StatefulWidget {
  @override
  _EmployerRegistrationFormState createState() =>
      _EmployerRegistrationFormState();
}

class _EmployerRegistrationFormState extends State<EmployerRegistrationForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  String _name;
  String _address;
  String _phoneNum;
  String _aadharNum;
  String _district;

  void _vaildateRegistration() async {
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      await _authService
          .signUpEmployerWithGoogle(_name, _address, _phoneNum, _aadharNum)
          .then((user) => {
                if (user != null)
                  {Navigator.popUntil(context, (route) => route.isFirst)}
                else
                  {
                    // TODO: Handle the errors properly
                    print('An Error Occured!')
                  }
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> _districts = [
      AppLocalizations.of(context).translate("Puducherry"),
      AppLocalizations.of(context).translate("Karaikal"),
      AppLocalizations.of(context).translate("Yanam"),
      AppLocalizations.of(context).translate("Mahe"),
    ];
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: _formKey,
        autovalidate: _autovalidate,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                AppLocalizations.of(context).translate('employer_registration'),
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('name'),
                      ),
                      onSaved: (input) {
                        _name = input;
                      },
                      validator: (value) =>
                          (value.isNotEmpty) ? null : 'Enter a Name',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('address'),
                      ),
                      onSaved: (input) {
                        _address = input;
                      },
                      validator: (value) =>
                          (value.isNotEmpty) ? null : 'Enter a Address',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('phone_no'),
                      ),
                      onSaved: (input) {
                        _phoneNum = '+91' + input;
                      },
                      validator: (value) => (value.length == 10)
                          ? null
                          : 'Enter a Valid Phone Number',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('aadhar'),
                      ),
                      onSaved: (input) {
                        _aadharNum = input;
                      },
                      validator: (value) =>
                          (value.isEmpty || value.length == 12)
                              ? null
                              : AppLocalizations.of(context)
                                  .translate('valid_aadhar'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 2),
                        child: DropdownButton(
                            underline: SizedBox(),
                            itemHeight: 50,
                            iconEnabledColor: Colors.deepOrange,
                            hint: Text('Select district'),
                            items: _districts.map((location) {
                              return DropdownMenuItem<String>(
                                child: Text(location),
                                value: location,
                              );
                            }).toList(),
                            value: _district,
                            onChanged: (String newValue) =>
                                setState(() => _district = newValue)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: BorderButton(
                label: AppLocalizations.of(context).translate('submit'),
                onPressed: _vaildateRegistration,
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
