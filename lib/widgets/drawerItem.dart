import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final String label;
  final IconData icon;
  final Color color;
  final Function onPressed;

  DrawerItem({
    @required this.onPressed,
    @required this.icon,
    @required this.label,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          border: Border.symmetric(
            vertical: BorderSide(color: Colors.deepOrangeAccent, width: 0.4),
          ),
        ),
        padding: EdgeInsets.all(10),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Icon(
                icon,
                color: color,
                size: 25,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(label,
                  style: TextStyle(
                    color: color,
                    fontSize: 18,
                  )),
            )
          ],
        ),
      ),
    );
  }
}
