import 'package:flutter/material.dart';
import 'package:hope/models/job.dart';
import 'package:hope/models/customUser.dart';
import 'package:hope/pages/jobDescription.dart';
import 'package:hope/widgets/jobListing.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JobsWrapper extends StatefulWidget {
  final String uid;
  String district;

  JobsWrapper({this.uid});

  @override
  _JobsWrapperState createState() => _JobsWrapperState();
}

class _JobsWrapperState extends State<JobsWrapper> {
  @override
  void initState() {
    _read();
  }

  void _read() async {
    final pref = await SharedPreferences.getInstance();
    final key = 'district';
    final value = pref.getString('district');
    widget.district = value;
    print('District is ${widget.district}');
  }

  @override
  Widget build(BuildContext context) {
    final jobs = Provider.of<List<Job>>(context);
    print("Shared preferences = ${widget.district}");
    return ListView.builder(
      itemCount: jobs.length,
      itemBuilder: (context, index) {
        return JobListing(
          job: jobs[index],
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => JobDescription(
                  uid: widget.uid,
                  title: jobs[index].title,
                  description: jobs[index].description,
                  phone: jobs[index].phone,
                  lat: jobs[index].lat,
                  long: jobs[index].long,
                  district: jobs[index].district,
                ),
              ),
            );
          },
        );
      },
    );
  }
}
