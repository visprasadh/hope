import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/pages/choosePage.dart';
import 'package:hope/models/app_localizations.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage('images/titleless.png'),
                    ),
                    // Text(
                    //   AppLocalizations.of(context).translate('HOPE'),
                    //   style: TextStyle(
                    //     fontSize: 72,
                    //     fontFamily: 'Montserrat',
                    //   ),
                    // ),
                  ),
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('login'),
                  onPressed: () {
                    AuthService _auth = AuthService();
                    try {
                      _auth.signInWithGoogle();
                    } catch (error) {
                      print(error);
                    }
                  },
                ),
                FlatButton(
                  child: Text(
                    AppLocalizations.of(context).translate('new_user'),
                    style: TextStyle(
                      color: Colors.deepOrange,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ChoosePage()));
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
