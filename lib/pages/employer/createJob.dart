import 'package:flutter/material.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/employer/locationPicker.dart';
import 'package:hope/services/firestoreService.dart';

class CreateJob extends StatefulWidget {
  final String uid;

  String title;
  String description;
  String name;
  String address;
  String phoneNum;
  String email;
  String postedBy;
  String district;
  double latitude = 21.494069;
  double longitude = 78.456259;

  CreateJob({
    this.uid,
    this.title,
    this.description,
    this.name,
    this.address,
    this.phoneNum,
    this.email,
    this.district,
  });

  CreateJob.location({
    this.uid,
    this.title,
    this.description,
    this.name,
    this.address,
    this.phoneNum,
    this.email,
    this.district,
    this.latitude,
    this.longitude,
  });

  @override
  _CreateJobState createState() => _CreateJobState();
}

class _CreateJobState extends State<CreateJob> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller;
  bool autovalidate = false;

  // String title;
  // String description;
  // String name;
  // String address;
  // String phoneNum;
  // String email;

  String _emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) return '*Required';
    if (!regex.hasMatch(value))
      return '*Enter a valid email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    List<String> _districts = [
      AppLocalizations.of(context).translate("Puducherry"),
      AppLocalizations.of(context).translate("Karaikal"),
      AppLocalizations.of(context).translate("Yanam"),
      AppLocalizations.of(context).translate("Mahe"),
    ];
    List<double> _latlng = new List(2);
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: formKey,
        autovalidate: autovalidate,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                AppLocalizations.of(context).translate('create_job'),
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: AppLocalizations.of(context).translate('title'),
                ),
                onSaved: (input) => widget.title = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                decoration: InputDecoration(
                  labelText:
                      AppLocalizations.of(context).translate('description'),
                  border: OutlineInputBorder(),
                ),
                onSaved: (input) => widget.description = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: AppLocalizations.of(context).translate('name'),
                ),
                onSaved: (input) => widget.name = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: AppLocalizations.of(context).translate('address'),
                ),
                onSaved: (input) => widget.address = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: AppLocalizations.of(context).translate('phone_no'),
                ),
                onSaved: (input) => widget.phoneNum = input,
                validator: (value) => (value.length >= 10 && value.length <= 13)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: AppLocalizations.of(context).translate('email'),
                ),
                onSaved: (input) => widget.email = input,
                validator: _emailValidator,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  child: DropdownButton(
                      underline: SizedBox(),
                      itemHeight: 50,
                      iconEnabledColor: Colors.deepOrange,
                      hint: Text('Select district'),
                      items: _districts.map((location) {
                        return DropdownMenuItem<String>(
                          child: Text(location),
                          value: location,
                        );
                      }).toList(),
                      value: widget.district,
                      onChanged: (String newValue) =>
                          setState(() => widget.district = newValue)),
                ),
              ),
            ),
            BorderButton(
              label: AppLocalizations.of(context).translate('add_location'),
              onPressed: () async {
                if (formKey.currentState.validate()) {
                  formKey.currentState.save();
                  _latlng = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LocationPicker(),
                    ),
                  );
                  setState(() {
                    widget.latitude = _latlng[0];
                    widget.longitude = _latlng[1];
                  });
                }
              },
            ),
            BorderButton(
              label: AppLocalizations.of(context).translate('submit'),
              onPressed: () {
                print(widget.district);
                print(widget.latitude);
                formKey.currentState.save();
                FirestoreService _store = FirestoreService();
                _store.postJob(
                    widget.uid,
                    widget.title,
                    widget.description,
                    widget.name,
                    widget.address,
                    widget.phoneNum,
                    widget.email,
                    widget.latitude,
                    widget.longitude,
                    widget.district);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    ));
  }
}
