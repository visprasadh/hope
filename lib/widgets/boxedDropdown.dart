import 'package:flutter/material.dart';
import 'package:hope/models/app_localizations.dart';

class BoxedDropDown extends StatefulWidget {
  @override
  _BoxedDropDownState createState() => _BoxedDropDownState();
  String district;
  BoxedDropDown({this.district});
}

class _BoxedDropDownState extends State<BoxedDropDown> {
  @override
  Widget build(BuildContext context) {
    List<String> _districts = [
      AppLocalizations.of(context).translate("Puducherry"),
      AppLocalizations.of(context).translate("Karaikal"),
      AppLocalizations.of(context).translate("Yanam"),
      AppLocalizations.of(context).translate("Mahe"),
    ];
    return Container(
      child:
      Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  child: DropdownButton(
                      underline: SizedBox(),
                      itemHeight: 50,
                      iconEnabledColor: Colors.deepOrange,
                      hint: Text('Select district'),
                      items: _districts.map((location) {
                        return DropdownMenuItem<String>(
                          child: Text(location),
                          value: location,
                        );
                      }).toList(),
                      value: widget.district,
                      onChanged: (String newValue) =>
                          setState(() => widget.district = newValue)),
                ),
              ),
            ),
      
    );
  }
}